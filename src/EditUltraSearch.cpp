#include "framework.h"

struct list_head	listNavigateBackNextTrace ;
int			nMaxNavigateBackNextTraceCount = 0 ;

HWND			hwndSearchFind ;
HWND			hwndEditBoxInSearchFind ;

HWND			hwndSearchFound ;
HWND			hwndListBoxInSearchFound ;

HWND			hwndSearchReplace ;
HWND			hwndFromEditBoxInSearchReplace ;
HWND			hwndToEditBoxInSearchReplace ;

void JumpGotoLine( struct TabPage *pnodeTabPage , int nGotoLineNo , int nCurrentLineNo )
{
	int nLineCount = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	int nScreenLineCount = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_LINESONSCREEN , 0 , 0 ) ;
	if( nGotoLineNo > nCurrentLineNo )
	{
		int nJumpLineNo = nGotoLineNo + nScreenLineCount / 2 ;
		if( nJumpLineNo > nLineCount )
			nJumpLineNo = nLineCount ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nJumpLineNo , 0 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nGotoLineNo , 0 );
	}
	else
	{
		int nJumpLineNo = nGotoLineNo - nScreenLineCount / 2 ;
		if( nJumpLineNo < 1 )
			nJumpLineNo = 1 ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nJumpLineNo , 0 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GOTOLINE , nGotoLineNo , 0 );
	}

	return;
}

static size_t PrepareSearchFlags( HWND hwndSearch )
{
	size_t		flags = 0 ;

	if( IsDlgButtonChecked( hwndSearch , IDC_OPTIONS_WHOLEWORD ) )
		flags |= SCFIND_WHOLEWORD ;
	if( IsDlgButtonChecked( hwndSearch , IDC_OPTIONS_MATCHCASE ) )
		flags |= SCFIND_MATCHCASE ;
	if( IsDlgButtonChecked( hwndSearch , IDC_OPTIONS_WORDSTART ) )
		flags |= SCFIND_WORDSTART ;

	if( IsDlgButtonChecked( hwndSearch , IDC_TEXTTYPE_REGEXP ) )
		flags |= SCFIND_REGEXP ;
	/*
	else if( IsDlgButtonChecked( hwndSearch , IDC_TEXTTYPE_POSIXREGEXP ) )
		flags |= SCFIND_POSIX ;
	*/

	return flags;
}

static int AdjustCurrentPosBeforeFindPrev()
{
	size_t nSelectStartPos=(size_t)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	size_t nSelectEndPos=(size_t)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	if( nSelectEndPos != nSelectStartPos )
	{
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETCURRENTPOS , nCurrentPos-1 , 0 ) ;
	}
	return nCurrentPos;
}

static int AdjustCurrentPosBeforeFindNext()
{
	int nSelectStartPos=(int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 );
	int nSelectEndPos=(int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 );
	int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	if( nSelectEndPos != nSelectStartPos )
	{
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETCURRENTPOS , nCurrentPos+1 , 0 ) ;
	}

	return nCurrentPos;
}

static char *GetWindowString( HWND hWnd )
{
	int	nTextLen ;
	char	*pcText = NULL ;

	nTextLen = (int)::SendMessage( hWnd , WM_GETTEXTLENGTH , 0 , 0 ) ;
	if( nTextLen == 0 )
		return NULL;
	pcText = (char*)malloc( nTextLen+1) ;
	if( pcText == NULL )
		return NULL;
	memset( pcText , 0x00 , nTextLen+1 );
	::SendMessage( hWnd , WM_GETTEXT , nTextLen+1 , (LPARAM)pcText );

	return pcText;
}

static void SetWindowString( HWND hWnd , char *pcText )
{
	::SendMessage( hWnd , WM_SETTEXT , -1 , (LPARAM)pcText );
}

void SyncReplaceOptionsToFindDialog()
{
	::CheckDlgButton( hwndSearchFind , IDC_TEXTTYPE_GENERAL , IsDlgButtonChecked( hwndSearchReplace , IDC_TEXTTYPE_GENERAL ) );
	::CheckDlgButton( hwndSearchFind , IDC_TEXTTYPE_REGEXP , IsDlgButtonChecked( hwndSearchReplace , IDC_TEXTTYPE_REGEXP ) );

	::CheckDlgButton( hwndSearchFind , IDC_AREATYPE_THISFILE , IsDlgButtonChecked( hwndSearchReplace , IDC_AREATYPE_THISFILE ) );
	::CheckDlgButton( hwndSearchFind , IDC_AREATYPE_ALLOPENEDFILES , IsDlgButtonChecked( hwndSearchReplace , IDC_AREATYPE_ALLOPENEDFILES ) );

	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_WHOLEWORD , IsDlgButtonChecked( hwndSearchReplace , IDC_OPTIONS_WHOLEWORD ) );
	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_MATCHCASE , IsDlgButtonChecked( hwndSearchReplace , IDC_OPTIONS_MATCHCASE ) );
	::CheckDlgButton( hwndSearchFind , IDC_OPTIONS_WORDSTART , IsDlgButtonChecked( hwndSearchReplace , IDC_OPTIONS_WORDSTART ) );

	::CheckDlgButton( hwndSearchFind , IDC_SEARCHLOCATE_TO_TOP_OR_BOTTOM , IsDlgButtonChecked( hwndSearchReplace , IDC_SEARCHLOCATE_TO_TOP_OR_BOTTOM ) );
	::CheckDlgButton( hwndSearchFind , IDC_SEARCHLOCATE_LOOP , IsDlgButtonChecked( hwndSearchReplace , IDC_SEARCHLOCATE_LOOP ) );

	return;
}

void SyncReplaceFromTextToFindDialog()
{
	char *pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
	::SendMessage( hwndEditBoxInSearchFind , WM_SETTEXT , 0 , (LPARAM)pcReplaceDialogFromText );
	free( pcReplaceDialogFromText );

	return;
}

static int GrepSomethingInCurrentDirectory( HWND hwnd , struct TabPage *pnodeTabPage , char *pcFindDialogText )
{
	int	found = 0 ;
	
	if( pnodeTabPage->stRemoteFileServer.acNetworkAddress[0] )
	{
		WarnBox( "该功能不支持在远程文件目录中搜索" );
		::CheckRadioButton( hwnd , IDC_AREATYPE_THISFILE , IDC_AREATYPE_CURRENTDIRECTORY , IDC_AREATYPE_ALLOPENEDFILES );
		return EUX_ERROR_GREP_NOT_FOUND;
	}

	char			acFindPathFilename[ MAX_PATH ] ;
	WIN32_FIND_DATA		stFindFileData ;
	HANDLE			hFindFile ;
	char			acPathFilename[ MAX_PATH ] ;
	struct DocTypeConfig	*pstDocTypeConfig = NULL ;
	char			acExtName[ MAX_PATH ] ;

	memset( acFindPathFilename , 0x00 , sizeof(acFindPathFilename) );
	snprintf( acFindPathFilename , sizeof(acFindPathFilename)-1 , "%s*.*" , pnodeTabPage->acPathName );

	hFindFile = ::FindFirstFile( acFindPathFilename , & stFindFileData ) ;
	if( hFindFile == INVALID_HANDLE_VALUE )
		return 0;

	do
	{
		if( strcmp(stFindFileData.cFileName,".") == 0 || strcmp(stFindFileData.cFileName,"..") == 0 )
			continue;

		if( stFindFileData.dwFileAttributes & FILE_ATTRIBUTE_ARCHIVE )
		{
			snprintf( acPathFilename , sizeof(acPathFilename)-1 , "%s%s" , pnodeTabPage->acPathName , stFindFileData.cFileName ) ;
			memset( acExtName , 0x00 , sizeof(acExtName) );
			SplitPathFilename( acPathFilename , NULL , NULL , NULL , NULL , acExtName , NULL , NULL );
			pstDocTypeConfig = GetDocTypeConfig( acExtName ) ;
			if( pstDocTypeConfig )
			{
				char *file_buf = filedup( acPathFilename , NULL ) ;
				if( file_buf )
				{
					if( strstr( file_buf , pcFindDialogText ) )
					{
						OpenFileDirectly( acPathFilename );
						found = 1 ;
					}
				}
			}
		}
	}
	while( ::FindNextFile( hFindFile , & stFindFileData ) );

	::FindClose( hFindFile );

	if( found )
	{
		SelectTabPage( pnodeTabPage , -1 );
		return 0;
	}
	else
	{
		return EUX_ERROR_GREP_NOT_FOUND;
	}
}

INT_PTR CALLBACK SearchFindWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	int		nret = 0 ;

	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if ( LOWORD(wParam) == IDCANCEL)
		{
			ShowWindow( hDlg , SW_HIDE );
			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_FINDPREV )
		{
			size_t	nFindFlags = PrepareSearchFlags( hwndSearchFind ) ;
			BOOL	bLoopFind = IsDlgButtonChecked( hwndSearchFind , IDC_SEARCHLOCATE_LOOP ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcFindDialogText = GetWindowString( hwndEditBoxInSearchFind ) ;
			if( pcFindDialogText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcFindDialogText) ;
				if( tmp == NULL )
				{
					free( pcFindDialogText );
					break;
				}
				pcFindDialogText = tmp ;
			}

			if( IsDlgButtonChecked( hwndSearchFind , IDC_AREATYPE_CURRENTDIRECTORY ) )
			{
				GrepSomethingInCurrentDirectory( hwndSearchFind , g_pnodeCurrentTabPage , pcFindDialogText );
				::CheckRadioButton( hwndSearchFind , IDC_AREATYPE_THISFILE , IDC_AREATYPE_CURRENTDIRECTORY , IDC_AREATYPE_ALLOPENEDFILES );
				free( pcEditorSelectionText );
				free( pcFindDialogText );
				return (INT_PTR)TRUE;
			}

			if( IsDlgButtonChecked( hwndSearchFind , IDC_AREATYPE_THISFILE ) )
			{
				Sci_TextToFind	ft ;
				int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				int nMaxPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
				int nStartPos = nCurrentPos - 1 ;
				int nEndPos = 0 ;
_GOTO_RETRY_FINDPREV_IN_THISFILE:
				memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcFindDialogText ;
				int nFoundPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFoundPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
				else
				{
					if( nCurrentPos < nMaxPos && bLoopFind == TRUE )
					{
						nStartPos = nMaxPos ;
						nEndPos = nCurrentPos ;
						bLoopFind = FALSE ;
						goto _GOTO_RETRY_FINDPREV_IN_THISFILE;
					}
					else
					{
						InfoBox( "搜到顶了" );
					}
				}
			}
			else
			{
				int		nTabPagesCount ;
				int		nCurrentTabPageIndex ;
				TCITEM		tci ;
				struct TabPage	*p = NULL ;

				nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
				for( nCurrentTabPageIndex = 0; nCurrentTabPageIndex < nTabPagesCount; nCurrentTabPageIndex++ )
				{
					memset( & tci , 0x00 , sizeof(TCITEM) );
					tci.mask = TCIF_PARAM ;
					TabCtrl_GetItem( g_hwndTabPages , nCurrentTabPageIndex , & tci );
					p = (struct TabPage *)(tci.lParam);
					if( p == g_pnodeCurrentTabPage )
					{
						Sci_TextToFind	ft ;
						int nCurrentPos = (int)p->pfuncScintilla( p->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
						int nMaxPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
						int nStartPos = nCurrentPos - 1 ;
						int nEndPos = 0 ;
						memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
						ft.chrg.cpMin = nStartPos ;
						ft.chrg.cpMax = nEndPos ;
						ft.lpstrText = pcFindDialogText ;
						int nFoundPos = (int)p->pfuncScintilla( p->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
						if( nFoundPos >= 0 )
						{
							p->pfuncScintilla( p->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
						}
						else
						{
							int nPrevTabPageIndex = nCurrentTabPageIndex - 1 ;
							for( ; ; nPrevTabPageIndex-- )
							{
								if( bLoopFind == FALSE && nPrevTabPageIndex < 0 )
								{
									InfoBox( "搜到顶了" );
									break;
								}

								if( nPrevTabPageIndex < 0 )
									nPrevTabPageIndex = nTabPagesCount - 1 ;

								memset( & tci , 0x00 , sizeof(TCITEM) );
								tci.mask = TCIF_PARAM ;
								TabCtrl_GetItem( g_hwndTabPages , nPrevTabPageIndex , & tci );
								p = (struct TabPage *)(tci.lParam);

								memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
								ft.chrg.cpMin = (int)p->pfuncScintilla( p->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
								if( nPrevTabPageIndex != nCurrentTabPageIndex )
									ft.chrg.cpMax = 0 ;
								else
									ft.chrg.cpMax = nCurrentPos ;
								ft.lpstrText = pcFindDialogText ;
								nFoundPos = (int)p->pfuncScintilla( p->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
								if( nFoundPos >= 0 )
								{
									SelectTabPageByIndex( nPrevTabPageIndex );
									p->pfuncScintilla( p->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
									break;
								}
								else
								{
									if( nPrevTabPageIndex == nCurrentTabPageIndex )
									{
										InfoBox( "搜到底了" );
										break;
									}
								}
							}
						}
						break;
					}
				}
			}

			free( pcEditorSelectionText );
			free( pcFindDialogText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_FINDNEXT )
		{
			size_t	nFindFlags = PrepareSearchFlags( hwndSearchFind ) ;
			BOOL	bLoopFind = IsDlgButtonChecked( hwndSearchFind , IDC_SEARCHLOCATE_LOOP ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcFindDialogText = GetWindowString( hwndEditBoxInSearchFind ) ;
			if( pcFindDialogText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcFindDialogText) ;
				if( tmp == NULL )
				{
					free( pcFindDialogText );
					break;
				}
				pcFindDialogText = tmp ;
			}

			if( IsDlgButtonChecked( hwndSearchFind , IDC_AREATYPE_CURRENTDIRECTORY ) )
			{
				GrepSomethingInCurrentDirectory( hwndSearchFind , g_pnodeCurrentTabPage , pcFindDialogText );
				::CheckRadioButton( hwndSearchFind , IDC_AREATYPE_THISFILE , IDC_AREATYPE_CURRENTDIRECTORY , IDC_AREATYPE_ALLOPENEDFILES );
				free( pcEditorSelectionText );
				free( pcFindDialogText );
				return (INT_PTR)TRUE;
			}

			if( IsDlgButtonChecked( hwndSearchFind , IDC_AREATYPE_THISFILE ) )
			{
				Sci_TextToFind	ft ;
				int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
				int nMaxPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
				int nStartPos = nCurrentPos + 1 ;
				int nEndPos = nMaxPos ;
_GOTO_RETRY_FINDNEXT_IN_THISFILE:
				memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcFindDialogText ;
				int nFoundPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFoundPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
				else
				{
					if( nCurrentPos > 0 && bLoopFind == TRUE )
					{
						nStartPos = 0 ;
						nEndPos = nCurrentPos ;
						bLoopFind = FALSE ;
						goto _GOTO_RETRY_FINDNEXT_IN_THISFILE;
					}
					else
					{
						InfoBox( "搜到底了" );
					}
				}
			}
			else
			{
				int		nTabPagesCount ;
				int		nCurrentTabPageIndex ;
				TCITEM		tci ;
				struct TabPage	*p = NULL ;

				nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
				for( nCurrentTabPageIndex = 0; nCurrentTabPageIndex < nTabPagesCount; nCurrentTabPageIndex++ )
				{
					memset( & tci , 0x00 , sizeof(TCITEM) );
					tci.mask = TCIF_PARAM ;
					TabCtrl_GetItem( g_hwndTabPages , nCurrentTabPageIndex , & tci );
					p = (struct TabPage *)(tci.lParam);
					if( p == g_pnodeCurrentTabPage )
					{
						Sci_TextToFind	ft ;
						int nCurrentPos = (int)p->pfuncScintilla( p->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
						int nMaxPos = (int)p->pfuncScintilla( p->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
						int nStartPos = nCurrentPos + 1 ;
						int nEndPos = nMaxPos ;
						memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
						ft.chrg.cpMin = nStartPos ;
						ft.chrg.cpMax = nEndPos ;
						ft.lpstrText = pcFindDialogText ;
						int nFoundPos = (int)p->pfuncScintilla( p->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
						if( nFoundPos >= 0 )
						{
							p->pfuncScintilla( p->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
						}
						else
						{
							int nNextTabPageIndex = nCurrentTabPageIndex + 1 ; 
							for( ; ; nNextTabPageIndex++ )
							{
								if( bLoopFind == FALSE && nNextTabPageIndex >= nTabPagesCount )
								{
									InfoBox( "搜到底了" );
									break;
								}

								if( nNextTabPageIndex == nTabPagesCount )
									nNextTabPageIndex = 0 ;

								memset( & tci , 0x00 , sizeof(TCITEM) );
								tci.mask = TCIF_PARAM ;
								TabCtrl_GetItem( g_hwndTabPages , nNextTabPageIndex , & tci );
								p = (struct TabPage *)(tci.lParam);

								memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
								ft.chrg.cpMin = 0 ;
								if( nNextTabPageIndex != nCurrentTabPageIndex )
									ft.chrg.cpMax = (int)p->pfuncScintilla( p->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) - 1 ;
								else
									ft.chrg.cpMax = nCurrentPos ;
								ft.lpstrText = pcFindDialogText ;
								nFoundPos = (int)p->pfuncScintilla( p->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
								if( nFoundPos >= 0 )
								{
									SelectTabPageByIndex( nNextTabPageIndex );
									p->pfuncScintilla( p->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
									break;
								}
								else
								{
									if( nNextTabPageIndex == nCurrentTabPageIndex )
									{
										InfoBox( "搜到底了" );
										break;
									}
								}
							}
						}
						break;
					}
				}
			}

			free( pcEditorSelectionText );
			free( pcFindDialogText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_FOUNDLIST )
		{
			if( IsDlgButtonChecked( hwndSearchFind , IDC_AREATYPE_THISFILE ) == FALSE )
			{
				InfoBox( "目前只支持当前文件处理" );
				break;
			}

			size_t	nFindFlags = PrepareSearchFlags( hwndSearchFind ) ;
			char	*pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char	*pcFindDialogText = GetWindowString( hwndEditBoxInSearchFind ) ;
			if( pcFindDialogText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcFindDialogText) ;
				if( tmp == NULL )
				{
					free( pcFindDialogText );
					break;
				}
				pcFindDialogText = tmp ;
			}

			int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			int nCurrentLine = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_LINEFROMPOSITION , nCurrentPos , 0 ) ;

			int nTextTotalLength = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
			char *pcText = (char*)malloc( nTextTotalLength + 1 ) ;
			if( pcText == NULL )
				return -1;
			memset( pcText , 0x00 , nTextTotalLength + 1 );
			g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETTEXT , (sptr_t)(nTextTotalLength+1) , (sptr_t)pcText ) ;

			ListBox_ResetContent( hwndListBoxInSearchFound );

			int linenumber = -1 ;
			char *linestart = pcText ;
			char *lineend = strchr( linestart , '\n' ) ;
			char *p = NULL ;
			int nCurrentListIndex = -1 ;
			char acAddString[ 4096 ] ;
			while( linestart )
			{
				if( lineend )
					(*lineend) = '\0' ;
				int linelen = (int)strlen(linestart) ;
				if( linestart[linelen-1] == '\r' )
					linestart[linelen-1] = '\0' ;
				linelen--;

				for( p = linestart ; (*p) ; p++ )
				{
					if( (*p) == '\t' )
						(*p) = ' ' ;
				}

				linenumber++;

				if( strstr( linestart , pcFindDialogText ) )
				{
					memset( acAddString , 0x00 , sizeof(acAddString) );
					if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
					{
						char *tmp = StrdupUtf8ToGb(linestart) ;
						if( tmp == NULL )
						{
							break;
						}
						snprintf( acAddString , sizeof(acAddString)-1 , "%d: %s" , linenumber+1 , tmp );
						free( tmp );
					}
					else
					{
						snprintf( acAddString , sizeof(acAddString)-1 , "%d: %s" , linenumber+1 , linestart );
					}

					int nListBoxItemIndex = ListBox_AddString( hwndListBoxInSearchFound , acAddString ) ;
					ListBox_SetItemData( hwndListBoxInSearchFound , nListBoxItemIndex , (LPARAM)linenumber );
					if( linenumber == nCurrentLine )
						nCurrentListIndex = ListBox_GetCount( hwndListBoxInSearchFound ) - 1 ;
				}
				
				if( lineend == NULL )
					break;
				linestart = lineend + 1 ;
				lineend = strchr( linestart , '\n' ) ;
			}

			if( nCurrentListIndex >= 0 )
				ListBox_SetCurSel( hwndListBoxInSearchFound , nCurrentListIndex );

			free( pcText );

			free( pcEditorSelectionText );
			free( pcFindDialogText );

			if( ListBox_GetCount(hwndListBoxInSearchFound) > 0 )
			{
				CenterWindow( hwndSearchFound , g_hwndMainWindow );
				ShowWindow( hwndSearchFound , SW_SHOW );
				UpdateWindow( hwndSearchFound );
			}
			else
			{
				ErrorBox( "没有匹配的行" );
			}

			return (INT_PTR)TRUE;
		}
		else
		{
			return (INT_PTR)TRUE;
		}
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK SearchReplaceWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if ( LOWORD(wParam) == IDCANCEL)
		{
			ShowWindow( hDlg , SW_HIDE );
			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_REPLACEPREV )
		{
			size_t nFindFlags = PrepareSearchFlags( hwndSearchReplace ) ;
			char *pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char *pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
			if( pcReplaceDialogFromText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogFromText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogFromText );
					break;
				}
				pcReplaceDialogFromText = tmp ;
			}
			char *pcReplaceDialogToText = GetWindowString( hwndToEditBoxInSearchReplace ) ;
			if( pcReplaceDialogToText == NULL )
			{
				pcReplaceDialogToText = _strdup("") ;
			}
			if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogToText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogToText );
					break;
				}
				pcReplaceDialogToText = tmp ;
			}

			if( pcEditorSelectionText == NULL || ( pcEditorSelectionText && ( strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 || strcmp( pcEditorSelectionText , pcReplaceDialogToText ) == 0 ) ) )
			{
				if( pcEditorSelectionText && strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );
				}

				SyncReplaceOptionsToFindDialog();
				SyncReplaceFromTextToFindDialog();
				::SendMessage( hwndSearchFind , WM_COMMAND , ID_FINDPREV , 0 );
			}
			else
			{
				Sci_TextToFind	ft ;
				int nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = 0 ;
				ft.lpstrText = pcReplaceDialogFromText ;
				int nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}

			free( pcReplaceDialogFromText );
			free( pcReplaceDialogToText );
			free( pcEditorSelectionText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_REPLACENEXT )
		{
			size_t nFindFlags = PrepareSearchFlags( hwndSearchReplace ) ;
			char *pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char *pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
			if( pcReplaceDialogFromText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogFromText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogFromText );
					break;
				}
				pcReplaceDialogFromText = tmp ;
			}
			char *pcReplaceDialogToText = GetWindowString( hwndToEditBoxInSearchReplace ) ;
			if( pcReplaceDialogToText == NULL )
			{
				pcReplaceDialogToText = _strdup("") ;
			}
			if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogToText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogToText );
					break;
				}
				pcReplaceDialogToText = tmp ;
			}

			if( pcEditorSelectionText == NULL || ( pcEditorSelectionText && ( strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 || strcmp( pcEditorSelectionText , pcReplaceDialogToText ) == 0 ) ) )
			{
				if( pcEditorSelectionText && strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );
				}

				SyncReplaceOptionsToFindDialog();
				SyncReplaceFromTextToFindDialog();
				::SendMessage( hwndSearchFind , WM_COMMAND , ID_FINDNEXT , 0 );
			}
			else
			{
				Sci_TextToFind	ft ;
				int nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				int nEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 ) ;
				memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
				ft.chrg.cpMin = nStartPos ;
				ft.chrg.cpMax = nEndPos ;
				ft.lpstrText = pcReplaceDialogFromText ;
				int	nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
				if( nFindPos >= 0 )
				{
					g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
				}
			}

			free( pcReplaceDialogFromText );
			free( pcReplaceDialogToText );
			free( pcEditorSelectionText );

			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_REPLACEALL )
		{
			size_t nFindFlags = PrepareSearchFlags( hwndSearchReplace ) ;
			char *pcEditorSelectionText = StrdupEditorSelection( NULL , 0 ) ;
			char *pcReplaceDialogFromText = GetWindowString( hwndFromEditBoxInSearchReplace ) ;
			if( pcReplaceDialogFromText == NULL )
			{
				if( pcEditorSelectionText )
					free( pcEditorSelectionText );
				break;
			}
			if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogFromText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogFromText );
					break;
				}
				pcReplaceDialogFromText = tmp ;
			}
			char *pcReplaceDialogToText = GetWindowString( hwndToEditBoxInSearchReplace ) ;
			if( pcReplaceDialogToText == NULL )
			{
				pcReplaceDialogToText = _strdup("") ;
			}
			if( g_pnodeCurrentTabPage->nCodePage == ENCODING_UTF8 )
			{
				char *tmp = StrdupGbToUtf8(pcReplaceDialogToText) ;
				if( tmp == NULL )
				{
					free( pcReplaceDialogToText );
					break;
				}
				pcReplaceDialogToText = tmp ;
			}

			if( pcEditorSelectionText == NULL || ( pcEditorSelectionText && ( strcmp( pcEditorSelectionText , pcReplaceDialogFromText ) == 0 || strcmp( pcEditorSelectionText , pcReplaceDialogToText ) == 0 ) ) )
			{
				Sci_TextToFind	ft ;
				int nStartPos ;

				g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_BEGINUNDOACTION , 0 , 0 );

				if( IsDlgButtonChecked( hwndSearchReplace , IDC_AREATYPE_THISFILE ) )
				{
					nStartPos = 0 ;
					while(1)
					{
						memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
						ft.chrg.cpMin = nStartPos ;
						ft.chrg.cpMax = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
						ft.lpstrText = pcReplaceDialogFromText ;
						int nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
						if( nFindPos >= 0 )
						{
							g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
							g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );

							nStartPos = nFindPos + 1 ;
						}
						else
						{
							break;
						}
					}
				}
				else
				{
					int		nTabPagesCount ;
					int		nTabPageIndex ;
					TCITEM		tci ;
					struct TabPage	*p = NULL ;

					nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
					for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
					{
						memset( & tci , 0x00 , sizeof(TCITEM) );
						tci.mask = TCIF_PARAM ;
						TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
						p = (struct TabPage *)(tci.lParam);

						nStartPos = 0 ;
						while(1)
						{
							memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
							ft.chrg.cpMin = nStartPos ;
							ft.chrg.cpMax = (int)p->pfuncScintilla( p->pScintilla , SCI_GETTEXTLENGTH , 0 , 0 ) ;
							ft.lpstrText = pcReplaceDialogFromText ;
							int nFindPos = (int)p->pfuncScintilla( p->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
							if( nFindPos >= 0 )
							{
								p->pfuncScintilla( p->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
								p->pfuncScintilla( p->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );

								nStartPos = nFindPos + 1 ;
							}
							else
							{
								break;
							}
						}
					}
				}

				g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_ENDUNDOACTION , 0 , 0 );
			}
			else
			{
				Sci_TextToFind	ft ;
				int nStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;
				int nEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETSELECTIONEND , 0 , 0 ) ;
				int nFileEndPos = nEndPos ;
				int nDeltaLen = (int)strlen(pcReplaceDialogToText) - (int)strlen(pcReplaceDialogFromText) ;
				if( nDeltaLen < 0 )
					nDeltaLen = 0 ;

				g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_BEGINUNDOACTION , 0 , 0 );

				while(1)
				{
					memset( & ft , 0x00 , sizeof(Sci_TextToFind) );
					ft.chrg.cpMin = nStartPos ;
					ft.chrg.cpMax = nEndPos ;
					ft.lpstrText = pcReplaceDialogFromText ;
					int nFindPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_FINDTEXT , nFindFlags , (sptr_t) & ft ) ;
					if( nFindPos >= 0 )
					{
						g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_SETSEL , ft.chrgText.cpMin , ft.chrgText.cpMax );
						g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)pcReplaceDialogToText );

						nStartPos = nFindPos + 1 ;
						nEndPos += nDeltaLen ;
						if( nEndPos > nFileEndPos )
							nEndPos = nFileEndPos ;
					}
					else
					{
						break;
					}
				}

				g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_ENDUNDOACTION , 0 , 0 );
			}

			free( pcReplaceDialogFromText );
			free( pcReplaceDialogToText );
			free( pcEditorSelectionText );
		}
		else
		{
			return (INT_PTR)TRUE;
		}
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK SearchFoundWndProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if( LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL )
		{
			ShowWindow( hDlg , SW_HIDE );
			return (INT_PTR)TRUE;
		}
		else if( LOWORD(wParam) == ID_COPYTO_CLIPBOARD )
		{
			int nItemCount = ListBox_GetCount( hwndListBoxInSearchFound ) ;
			if( nItemCount > 0 )
			{
				int nItemIndex ;
				int nTotalLength = 0 ;
				for( nItemIndex = 0 ; nItemIndex < nItemCount ; nItemIndex++ )
				{
					nTotalLength += ListBox_GetTextLen( hwndListBoxInSearchFound , nItemIndex ) ;
					nTotalLength += 2 ;
				}

				HGLOBAL hgl = (char*)GlobalAlloc( GMEM_MOVEABLE , nTotalLength + 1 ) ;
				char *acTotalText = (char*)GlobalLock(hgl) ;
				if( acTotalText == NULL )
				{
					ErrorBox( "不能分配内存用以存放复制到剪贴板的临时缓冲区" );
					return (INT_PTR)TRUE;
				}
				memset( acTotalText , 0x00 , nTotalLength + 1 );
				nTotalLength = 0 ;
				for( nItemIndex = 0 ; nItemIndex < nItemCount ; nItemIndex++ )
				{
					ListBox_GetText( hwndListBoxInSearchFound , nItemIndex , acTotalText+nTotalLength );
					nTotalLength += ListBox_GetTextLen( hwndListBoxInSearchFound , nItemIndex ) ;
					memcpy( acTotalText+nTotalLength , "\r\n" , 2 );
					nTotalLength += 2 ;
				}
				GlobalUnlock(hgl);

				::OpenClipboard( g_hwndMainWindow );
				::EmptyClipboard();
				::SetClipboardData( CF_TEXT , acTotalText );
				::CloseClipboard();
			}
		}
		else if( LOWORD(wParam) == IDC_FOUNDLIST && HIWORD(wParam) == LBN_DBLCLK )
		{
			int nListBoxItemNo = (int)SendMessage( hwndListBoxInSearchFound , LB_GETCURSEL , 0 , 0 ) ;
			int nGotoLineNo = (int)SendMessage( hwndListBoxInSearchFound , LB_GETITEMDATA , nListBoxItemNo , 0 ) ;
			int nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
			int nCurrentLineNo = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_LINEFROMPOSITION , nCurrentPos , 0 ) ;
			JumpGotoLine( g_pnodeCurrentTabPage , nGotoLineNo , nCurrentLineNo );

			return (INT_PTR)TRUE;
		}
		else
		{
			return (INT_PTR)TRUE;
		}
	}
	return (INT_PTR)FALSE;
}

int OnSearchFind( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		CopyEditorSelectionToWnd( pnodeTabPage , hwndEditBoxInSearchFind );

		::SendMessage( hwndSearchReplace , DM_SETDEFID , ID_FINDNEXT , 0 );
		::SetFocus( hwndEditBoxInSearchFind );

		CenterWindow( hwndSearchFind , g_hwndMainWindow );
		ShowWindow( hwndSearchFind , SW_SHOW );
	}

	return 0;
}

int OnSearchFindPrev( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		CopyEditorSelectionToWnd( pnodeTabPage , hwndEditBoxInSearchFind );

		::SendMessage( hwndSearchFind , WM_COMMAND , ID_FINDPREV , 0 );
	}

	return 0;
}

int OnSearchFindNext( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		CopyEditorSelectionToWnd( pnodeTabPage , hwndEditBoxInSearchFind );

		::SendMessage( hwndSearchFind , WM_COMMAND , ID_FINDNEXT , 0 );
	}

	return 0;
}

int OnSearchFoundList( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		CopyEditorSelectionToWnd( pnodeTabPage , hwndEditBoxInSearchFind );

		::SendMessage( hwndSearchFind , WM_COMMAND , ID_FOUNDLIST , 0 );
	}

	return 0;
}

int OnSearchReplace( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		CopyEditorSelectionToWnd( pnodeTabPage , hwndFromEditBoxInSearchReplace );

		::SendMessage( hwndSearchReplace , DM_SETDEFID , ID_REPLACENEXT , 0 );
		::SetFocus( hwndFromEditBoxInSearchReplace );

		CenterWindow( hwndSearchReplace , g_hwndMainWindow );
		ShowWindow( hwndSearchReplace , SW_SHOW );
	}

	return 0;
}

int OnSelectAll( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_SELECTALL, 0, 0 );
	}

	return 0;
}

int OnSelectWord( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		int	nCurrentPos ;
		int	nCurrentWordStartPos ;
		int	nCurrentWordEndPos ;

		nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentWordStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDSTARTPOSITION, nCurrentPos, true ) ;
		nCurrentWordEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDENDPOSITION, nCurrentPos, true ) ;
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_SETSEL, nCurrentWordStartPos, nCurrentWordEndPos ) ;
	}

	return 0;
}

int OnSelectLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
	{
		int	nCurrentPos ;
		int	nCurrentLine ;
		int	nCurrentLineStartPos ;
		int	nCurrentLineEndPos ;
		int	nEolMode ;

		nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
		nCurrentLineStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
		nCurrentLineEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETLINEENDPOSITION, nCurrentLine, 0 ) ;
		nEolMode = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETEOLMODE, 0, 0 ) ;
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_SETSEL, nCurrentLineStartPos, nCurrentLineEndPos+(nEolMode==0?2:1) ) ;
	}

	return 0;
}

int OnAddSelectLeftCharGroup( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDPARTLEFTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectRightCharGroup( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDPARTRIGHTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectLeftWord( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDLEFTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectRightWord( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDRIGHTEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectTopBlockFirstLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_PARAUPEXTEND, 0, 0 );
	return 0;
}

int OnAddSelectBottomBlockFirstLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_PARADOWNEXTEND, 0, 0 );
	return 0;
}

int OnMoveLeftCharGroup( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDPARTLEFT, 0, 0 );
	return 0;
}

int OnMoveRightCharGroup( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDPARTRIGHT, 0, 0 );
	return 0;
}

int OnMoveLeftWord( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDLEFT, 0, 0 );
	return 0;
}

int OnMoveRightWord( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDRIGHT, 0, 0 );
	return 0;
}

int OnMoveTopBlockFirstLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_PARAUP, 0, 0 );
	return 0;
}

int OnMoveBottomBlockFirstLine( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage )
		g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_PARADOWN, 0, 0 );
	return 0;
}

int OnSearchGotoLine( struct TabPage *pnodeTabPage )
{
	char		LineNo[ 20 ] ;

	int		nret = 0 ;

	if( pnodeTabPage == NULL )
		return 0;

	memset( LineNo , 0x00 , sizeof(LineNo) );
	strcpy( LineNo , "0" );
	nret = InputBox( g_hwndMainWindow , "请输入行号：" , "输入窗口" , 0 , LineNo , sizeof(LineNo)-1 ) ;
	if( nret == IDOK )
	{
		int	nGotoLine ;
		int	nCurrentPos ;
		int	nCurrentLine ;

		nGotoLine = atoi(LineNo) ;

		nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
		nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;

		JumpGotoLine( pnodeTabPage , nGotoLine-1 , nCurrentLine );
	}

	return 0;
}

int OnSearchToggleBookmark( struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nCurrentLine ;
	int	nMarkNumber ;

	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	nMarkNumber = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERGET , nCurrentLine , 0 ) ;
	if( nMarkNumber == MARGIN_BOOKMARK_VALUE )
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERADD , nCurrentLine , MARGIN_BOOKMARK_VALUE );
	}
	else
	{
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDELETE , nCurrentLine , MARGIN_BOOKMARK_VALUE );
	}

	return 0;
}

int OnSearchAddBookmark( struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nCurrentLine ;

	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERADD , nCurrentLine , MARGIN_BOOKMARK_VALUE );

	return 0;
}

int OnSearchRemoveBookmark( struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nCurrentLine ;

	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERDELETE , nCurrentLine , MARGIN_BOOKMARK_VALUE );

	return 0;
}

int OnSearchRemoveAllBookmarks( struct TabPage *pnodeTabPage )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_MARKERDELETEALL , MARGIN_BOOKMARK_VALUE , 0 );
	}

	return 0;
}

int OnSearchGotoPrevBookmark( struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nCurrentLine ;
	int	nFindLineNo ;

	if( pnodeTabPage == NULL )
		return 0;

	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	nFindLineNo = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERPREVIOUS , nCurrentLine-1 , MARGIN_BOOKMARK_MASKN ) ;
	if( nFindLineNo != -1 )
	{
		JumpGotoLine( pnodeTabPage , nFindLineNo , nCurrentLine );
	}

	return 0;
}

int OnSearchGotoNextBookmark( struct TabPage *pnodeTabPage )
{
	int	nCurrentPos ;
	int	nCurrentLine ;
	int	nFindLineNo ;

	if( pnodeTabPage == NULL )
		return 0;

	nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	nFindLineNo = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_MARKERNEXT , nCurrentLine+1 , MARGIN_BOOKMARK_MASKN ) ;
	if( nFindLineNo != -1 )
	{
		JumpGotoLine( pnodeTabPage , nFindLineNo , nCurrentLine );
	}

	return 0;
}

int OnSearchGotoPrevBookmarkInAllFiles( struct TabPage *pnodeTabPage )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	int		nCurrentPos ;
	int		nCurrentLine ;
	int		nFindLineNo ;
	int		nMaxLine ;
	
	if( pnodeTabPage == NULL )
		return 0;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		if( p == pnodeTabPage )
		{
			nCurrentPos = (int)p->pfuncScintilla( p->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
			nCurrentLine = (int)p->pfuncScintilla( p->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
			nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERPREVIOUS , nCurrentLine-1 , MARGIN_BOOKMARK_MASKN ) ;
			if( nFindLineNo != -1 )
			{
				JumpGotoLine( p , nFindLineNo , nCurrentLine );
				return 0;
			}
			else
			{
				for( nTabPageIndex-- ; nTabPageIndex >= 0 ; nTabPageIndex-- )
				{
					memset( & tci , 0x00 , sizeof(TCITEM) );
					tci.mask = TCIF_PARAM ;
					TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
					p = (struct TabPage *)(tci.lParam);

					nMaxLine = (int)p->pfuncScintilla( p->pScintilla, SCI_GETLINECOUNT, 0, 0 ) ;
					nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERPREVIOUS , nMaxLine-1 , MARGIN_BOOKMARK_MASKN ) ;
					if( nFindLineNo != -1 )
					{
						SelectTabPageByIndex( nTabPageIndex );
						JumpGotoLine( p , nFindLineNo , nMaxLine );
						return 0;
					}
				}
				return 1;
			}
		}
	}

	return 0;
}

int OnSearchGotoNextBookmarkInAllFiles( struct TabPage *pnodeTabPage )
{
	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	int		nCurrentPos ;
	int		nCurrentLine ;
	int		nFindLineNo ;

	if( pnodeTabPage == NULL )
		return 0;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		if( p == pnodeTabPage )
		{
			nCurrentPos = (int)p->pfuncScintilla( p->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
			nCurrentLine = (int)p->pfuncScintilla( p->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
			nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERNEXT , nCurrentLine+1 , MARGIN_BOOKMARK_MASKN ) ;
			if( nFindLineNo != -1 )
			{
				JumpGotoLine( p , nFindLineNo , nCurrentLine );
				return 0;
			}
			else
			{

				for( nTabPageIndex++ ; nTabPageIndex < nTabPagesCount ; nTabPageIndex++ )
				{
					memset( & tci , 0x00 , sizeof(TCITEM) );
					tci.mask = TCIF_PARAM ;
					TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
					p = (struct TabPage *)(tci.lParam);

					nFindLineNo = (int)p->pfuncScintilla( p->pScintilla , SCI_MARKERNEXT , 0 , MARGIN_BOOKMARK_MASKN ) ;
					if( nFindLineNo != -1 )
					{
						SelectTabPageByIndex( nTabPageIndex );
						JumpGotoLine( p , nFindLineNo , 0 );
						return 0;
					}
				}
				return 1;
			}
		}
	}

	return 0;
}

void InitNavigateBackNextTraceList()
{
	INIT_LIST_HEAD( & listNavigateBackNextTrace );
	nMaxNavigateBackNextTraceCount = 0 ;

	return;
}

int AddNavigateBackNextTrace( struct TabPage *pnodeTabPage , int nCurrentPos )
{
	if( nMaxNavigateBackNextTraceCount + 1 > MAX_TRACE_COUNT )
	{
		struct NavigateBackNextTrace *first = list_first_entry( & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace ) ;
		list_del( & (first->nodeNavigateBackNextTrace) );
		free( first );
		nMaxNavigateBackNextTraceCount--;
	}

	struct NavigateBackNextTrace *curr = (struct NavigateBackNextTrace*)malloc( sizeof(struct NavigateBackNextTrace) ) ;
	if( curr == NULL )
	{
		ErrorBox( "申请内存失败用以存放导航退回结构" );
		return -2;
	}
	memset( curr , 0x00 , sizeof(struct NavigateBackNextTrace) );

	curr->pnodeTabPage = pnodeTabPage ;
	curr->hwndScintilla = pnodeTabPage->hwndScintilla ;
	curr->nLastPos = nCurrentPos ;

	list_add_tail( & (curr->nodeNavigateBackNextTrace) , & listNavigateBackNextTrace );
	nMaxNavigateBackNextTraceCount++;

	return 0;
}

int UpdateNavigateBackNextTrace( struct TabPage *pnodeTabPage , int nCurrentPos )
{
	struct NavigateBackNextTrace *last = list_last_entry( & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace ) ;

	if( last && last->pnodeTabPage == pnodeTabPage )
		last->nLastPos = nCurrentPos ;
	
	return 0;
}

int OnSearchNavigateBackPrev_InThisFile()
{
	struct NavigateBackNextTrace	*curr = NULL ;
	struct NavigateBackNextTrace	*prev = NULL ;

	list_for_each_entry_safe_reverse( curr , prev , & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace )
	{
		if( curr->hwndScintilla != g_pnodeCurrentTabPage->hwndScintilla )
			return 1;
		
		list_del( & (curr->nodeNavigateBackNextTrace) );
		free( curr );
		nMaxNavigateBackNextTraceCount--;

		if( & (prev->nodeNavigateBackNextTrace) != & listNavigateBackNextTrace )
		{
			if( prev->hwndScintilla == g_pnodeCurrentTabPage->hwndScintilla )
			{
				int nTextLength = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETLENGTH, 0, 0 ) ;
				int nGotoPos ;
				if( prev->nLastPos > nTextLength - 1 )
					nGotoPos = nTextLength - 1 ;
				else
					nGotoPos = prev->nLastPos ;
				g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GOTOPOS , nGotoPos , 0 );
			}
		}

		return 0;
	}

	return 1;
}

int OnSearchNavigateBackPrev_InAllFiles()
{
	struct NavigateBackNextTrace	*curr = NULL ;
	struct NavigateBackNextTrace	*prev = NULL ;

	list_for_each_entry_safe_reverse( curr , prev , & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace )
	{
		if( curr->pnodeTabPage != g_pnodeCurrentTabPage )
		{
			SelectTabPage( curr->pnodeTabPage , -1 );
			return 0;
		}

		list_del( & (curr->nodeNavigateBackNextTrace) );
		free( curr );
		nMaxNavigateBackNextTraceCount--;

		if( & (prev->nodeNavigateBackNextTrace) != & listNavigateBackNextTrace )
		{
			if( prev->pnodeTabPage != g_pnodeCurrentTabPage )
			{
				SelectTabPage( prev->pnodeTabPage , -1 );
			}

			int nTextLength = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETLENGTH, 0, 0 ) ;
			int nGotoPos ;
			if( prev->nLastPos > nTextLength - 1 )
				nGotoPos = nTextLength - 1 ;
			else
				nGotoPos = prev->nLastPos ;
			g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla , SCI_GOTOPOS , nGotoPos , 0 );
		}

		return 0;
	}

	return 1;
}

void CleanNavigateBackNextTraceListByThisFile( struct TabPage *pnodeTabPage )
{
	struct NavigateBackNextTrace	*curr = NULL ;
	struct NavigateBackNextTrace	*next = NULL ;

	list_for_each_entry_safe( curr , next , & listNavigateBackNextTrace , struct NavigateBackNextTrace , nodeNavigateBackNextTrace )
	{
		if( curr->hwndScintilla == pnodeTabPage->hwndScintilla )
		{
			list_del( & (curr->nodeNavigateBackNextTrace) );
			free( curr );
			nMaxNavigateBackNextTraceCount--;
		}
	}

	return;
}

