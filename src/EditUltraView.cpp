#include "framework.h"

int OnViewFileTree( struct TabPage *pnodeTabPage )
{
	if( g_bIsFileTreeBarShow == FALSE )
	{
		g_bIsFileTreeBarShow = TRUE ;

		g_rectFileTreeBar.right = g_stEditUltraMainConfig.nFileTreeBarWidth ;
		g_rectTabPages.left = g_stEditUltraMainConfig.nFileTreeBarWidth + SPLIT_WIDTH ;
	}
	else
	{
		g_bIsFileTreeBarShow = FALSE ;

		g_rectFileTreeBar.right = 0 ;
		g_rectTabPages.left = 0 ;
	}

	UpdateAllWindows( g_hwndMainWindow );

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	return 0;
}

int OnViewSwitchFileType( int nFileTypeIndex )
{
	if( g_pnodeCurrentTabPage == NULL )
		return 0;

	CleanTabPageControls( g_pnodeCurrentTabPage );

	g_pnodeCurrentTabPage->pstDocTypeConfig = g_astDocTypeConfig + nFileTypeIndex ;

	InitTabPageControlsBeforeLoadFile( g_pnodeCurrentTabPage );

	InitTabPageControlsAfterLoadFile( g_pnodeCurrentTabPage );

	// 重新计算各窗口大小
	CalcTabPagesHeight();
	UpdateAllWindows( g_hwndMainWindow );

	return 0;
}

int OnViewSwitchStyleTheme( int nWindowThemeIndex )
{
	g_pstWindowTheme = & (g_astWindowTheme[nWindowThemeIndex]) ;

	if( nWindowThemeIndex == 0 )
		g_stEditUltraMainConfig.acWindowTheme[0] = '\0' ;
	else
		strncpy( g_stEditUltraMainConfig.acWindowTheme , g_pstWindowTheme->acThemeName , sizeof(g_stEditUltraMainConfig.acWindowTheme)-1 );

	g_brushCommonControlDarkColor = ::CreateSolidBrush( g_pstWindowTheme->stStyleTheme.text.bgcolor ) ;

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);

		InitTabPageControlsCommonStyle( p );

		InitTabPageControlsAfterLoadFile( p );

		if( p->hwndSymbolList )
		{
			HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 
			::SendMessage( p->hwndSymbolList , WM_SETFONT , (WPARAM)hFont, 0);
			::InvalidateRect( p->hwndSymbolList , NULL , TRUE );
		}

		if( p->hwndSymbolTree )
		{
			HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 
			::SendMessage( p->hwndSymbolTree , WM_SETFONT , (WPARAM)hFont, 0);
			::InvalidateRect( p->hwndSymbolTree , NULL , TRUE );
		}

		if( p->hwndQueryResultEdit )
		{
			HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 
			::SendMessage( p->hwndQueryResultEdit , WM_SETFONT , (WPARAM)hFont, 0);
			::InvalidateRect( p->hwndQueryResultEdit , NULL , TRUE );
		}

		if( p->hwndQueryResultTable )
		{
			HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 
			::SendMessage( p->hwndQueryResultTable , WM_SETFONT , (WPARAM)hFont, 0);
			::InvalidateRect( p->hwndQueryResultTable , NULL , TRUE );
		}
	}

	UpdateAllWindows( g_hwndMainWindow );

	UpdateAllMenus( g_hwndMainWindow , g_pnodeCurrentTabPage );

	return 0;
}

int OnViewModifyThemeStyle()
{
	int		nret = 0 ;

	nret = (int)DialogBox(g_hAppInstance, MAKEINTRESOURCE(IDD_STYLETHEME_DIALOG), g_hwndMainWindow, StyleThemeWndProc) ;
	if( nret == IDOK )
	{
		int		nTabPagesCount ;
		int		nTabPageIndex ;
		TCITEM		tci ;
		struct TabPage	*p = NULL ;

		nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
		for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
		{
			memset( & tci , 0x00 , sizeof(TCITEM) );
			tci.mask = TCIF_PARAM ;
			TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
			p = (struct TabPage *)(tci.lParam);

			InitTabPageControlsCommonStyle( p );

			InitTabPageControlsAfterLoadFile( p );
			
			if( p->hwndSymbolList )
			{
				HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 
				::SendMessage( p->hwndSymbolList , WM_SETFONT , (WPARAM)hFont, 0);
				::InvalidateRect( p->hwndSymbolList , NULL , TRUE );
			}

			if( p->hwndSymbolTree )
			{
				HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 
				::SendMessage( p->hwndSymbolTree , WM_SETFONT , (WPARAM)hFont, 0);
				::InvalidateRect( p->hwndSymbolTree , NULL , TRUE );
			}

			if( p->hwndQueryResultEdit )
			{
				HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 
				::SendMessage( p->hwndQueryResultEdit , WM_SETFONT , (WPARAM)hFont, 0);
				::InvalidateRect( p->hwndQueryResultEdit , NULL , TRUE );
			}

			if( p->hwndQueryResultTable )
			{
				HFONT hFont = CreateFont(-g_pstWindowTheme->stStyleTheme.text.fontsize-FONTSIZE_ZOOMOUT_ADJUSTVALUE, 0, 0, 0, (g_pstWindowTheme->stStyleTheme.text.bold?FW_BOLD:FW_NORMAL), false, FALSE, 0, DEFAULT_CHARSET , OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, g_pstWindowTheme->stStyleTheme.text.font); 
				::SendMessage( p->hwndQueryResultTable , WM_SETFONT , (WPARAM)hFont, 0);
				::InvalidateRect( p->hwndQueryResultTable , NULL , TRUE );
			}
		}

		SaveStyleThemeConfigFile();
	}

	return 0;
}

int OnViewCopyNewThemeStyle()
{
	char		acBoxCaption[ MAX_PATH*2 ] ;
	char		acStyleTheme[ MAX_PATH ] ;

	int		nret = 0 ;

	if( g_nWindowThemeCount >= VIEW_STYLETHEME_MAXCOUNT )
	{
		ErrorBox( "主题方案太多了" );
		return -1;
	}

	memset( acBoxCaption , 0x00 , sizeof(acBoxCaption) );
	snprintf( acBoxCaption , sizeof(acBoxCaption)-1 , "从主题方案\"%s\"复制创建到新主题方案：" , (g_pstWindowTheme==g_astWindowTheme?"":g_pstWindowTheme->acThemeName) );
	memset( acStyleTheme , 0x00 , sizeof(acStyleTheme) );
	strncpy( acStyleTheme , (g_pstWindowTheme==g_astWindowTheme?"":g_pstWindowTheme->acThemeName) , sizeof(acStyleTheme)-1 );
_GOTO_RETRY :
	nret = InputBox( g_hwndMainWindow , acBoxCaption , "输入窗口" , 0 , acStyleTheme , sizeof(acStyleTheme)-1 ) ;
	if( nret == IDOK )
	{
		if( acStyleTheme[0] == '\0' )
		{
			ErrorBox( "主题方案名不能为空" );
			goto _GOTO_RETRY;
		}
		else if( g_pstWindowTheme != g_astWindowTheme && strcmp( acStyleTheme , g_pstWindowTheme->acThemeName ) == 0 )
		{
			ErrorBox( "主题方案名不能和复制源相同" );
			goto _GOTO_RETRY;
		}
		else if( strchr(acStyleTheme,' ') || strchr(acStyleTheme,'\t') || strchr(acStyleTheme,'.') )
		{
			ErrorBox( "主题方案名不能含有' '或'\\t'或'.'" );
			goto _GOTO_RETRY;
		}
		else
		{
			nret = CopyNewThemeStyle( acStyleTheme ) ;
			if( nret )
				goto _GOTO_RETRY;
		}
	}

	return 0;
}

#define HEXEDIT_MODE_FIRSTLINE		"          | 0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F  | 0123456789ABCDEF\r\n"
#define HEXEDIT_MODE_SECONDLINE		"----------+-------------------------------------------------+-----------------\r\n"

int OnViewHexEditMode( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage->bHexEditMode == FALSE )
	{
		BOOL bIsDocumentModified = IsDocumentModified(pnodeTabPage) ;

		int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
		int nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
		int nOffsetInCurrentLine = nCurrentPos - (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
		int nTextTotalLength = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
		if( nTextTotalLength == 0 )
			return 0;
		int nTextTotalLineCount = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
		char *pcText = (char*)malloc( nTextTotalLength + 1 ) ;
		if( pcText == NULL )
			return -1;
		memset( pcText , 0x00 , nTextTotalLength + 1 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXT , (sptr_t)(nTextTotalLength+1) , (sptr_t)pcText ) ;

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_CLEARALL, 0, 0 );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_APPENDTEXT, sizeof(HEXEDIT_MODE_FIRSTLINE)-1, (sptr_t)HEXEDIT_MODE_FIRSTLINE );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_APPENDTEXT, sizeof(HEXEDIT_MODE_SECONDLINE)-1, (sptr_t)HEXEDIT_MODE_SECONDLINE );

		int nHexLineCount = (nTextTotalLength+15) / 16 ;
		pnodeTabPage->nByteCountInLastLine = nTextTotalLength % 16 ;
		if( pnodeTabPage->nByteCountInLastLine == 0 )
			pnodeTabPage->nByteCountInLastLine = 16 ;
		char *acHexText = (char*)malloc( nHexLineCount*80 + 1 ) ;
		if( acHexText == NULL )
			return -1;
		memset( acHexText , 0x00 , nHexLineCount*80 + 1 );
		char *pcHexText = acHexText ;
		size_t nHexTextLength = 0 ;
		size_t len ;

		int nHexLineBase = 0 ;
		char acHexLineOffsetBuf[ 8 + 1 + 1 ] = "" ;
		char acHexByte[ 3+1 + 1 + 1 ] = "" ;
		int x , y ;
		for( y = 0 ; y < nHexLineCount ; y++ )
		{
			len = snprintf( acHexLineOffsetBuf , sizeof(acHexLineOffsetBuf)-1 , "%08X" , nHexLineBase );
			memcpy( pcHexText , acHexLineOffsetBuf , len ); pcHexText += len ; nHexTextLength += len ;
			len = 4 ; memcpy( pcHexText , "H | " , len ); pcHexText += len ; nHexTextLength += len ;

			for( x = 0 ; x < 16 ; x++ )
			{
				if( y+1 == nHexLineCount && x >= pnodeTabPage->nByteCountInLastLine )
					break;

				len = snprintf( acHexByte , sizeof(acHexByte)-1 , "%02X " , (unsigned char)pcText[nHexLineBase+x] ) ;
				memcpy( pcHexText , acHexByte , len ); pcHexText += len ; nHexTextLength += len ;
			}
			for( ; x < 16 ; x++ )
			{
				len = 3 ; memcpy( pcHexText , "   " , len ); pcHexText += len ; nHexTextLength += len ;
			}

			pcHexText[0] = '|' ; pcHexText += 1 ; nHexTextLength += 1 ;
			pcHexText[0] = ' ' ; pcHexText += 1 ; nHexTextLength += 1 ;

			for( x = 0 ; x < 16 ; x++ )
			{
				if( y+1 == nHexLineCount && x >= pnodeTabPage->nByteCountInLastLine )
					break;

				if( pcText[nHexLineBase+x] && 32 <= pcText[nHexLineBase+x] && pcText[nHexLineBase+x] <= 126 )
				{
					pcHexText[0] = pcText[nHexLineBase+x] ; pcHexText += 1 ; nHexTextLength += 1 ;
				}
				else
				{
					pcHexText[0] = '.' ; pcHexText += 1 ; nHexTextLength += 1 ;
				}
			}
			for( ; x < 16 ; x++ )
			{
				pcHexText[0] = ' ' ; pcHexText += 1 ; nHexTextLength += 1 ;
			}

			if( y+1 < nHexLineCount )
			{
				pcHexText[0] = '\r' ; pcHexText += 1 ; nHexTextLength += 1 ;
				pcHexText[0] = '\n' ; pcHexText += 1 ; nHexTextLength += 1 ;
			}

			nHexLineBase += 16 ;
		}

		free( pcText );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_APPENDTEXT, nHexTextLength, (sptr_t)acHexText );

		int nAdujstPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine+2, 0 ) + 12 + nOffsetInCurrentLine*3 ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GOTOPOS, nAdujstPos, 0 );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_SETOVERTYPE, true, 0 );

		if( bIsDocumentModified == TRUE )
		{
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETSAVEPOINT, 0, 0);

			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_INSERTTEXT, 0, (sptr_t)"X" );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_DELETERANGE, 0, 1 );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

			OnSavePointLeft( pnodeTabPage );
		}
		else
		{
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETSAVEPOINT, 0, 0);

			OnSavePointReached( pnodeTabPage );
		}

		pnodeTabPage->bHexEditMode = TRUE ;
		UpdateAllMenus( g_hwndMainWindow , g_pnodeCurrentTabPage );
	}
	else
	{
		BOOL bIsDocumentModified = IsDocumentModified(pnodeTabPage) ;

		size_t nTextLength = 0 ;
		unsigned char *pcText = StrdupHexEditTextFold( pnodeTabPage , & nTextLength ) ;
		if( pcText == NULL )
			return -1;

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_CLEARALL, 0, 0 );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_APPENDTEXT, nTextLength, (sptr_t)pcText );

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_SETOVERTYPE, false, 0 );
		
		if( bIsDocumentModified == TRUE )
		{
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETSAVEPOINT, 0, 0);

			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_BEGINUNDOACTION, 0, 0 );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_INSERTTEXT, 0, (sptr_t)"X" );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_DELETERANGE, 0, 1 );
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_ENDUNDOACTION, 0, 0 );

			OnSavePointLeft( pnodeTabPage );
		}
		else
		{
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , EM_EMPTYUNDOBUFFER, 0, 0);
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETSAVEPOINT, 0, 0);

			OnSavePointReached( pnodeTabPage );
		}

		pnodeTabPage->bHexEditMode = FALSE ;
		UpdateAllMenus( g_hwndMainWindow , g_pnodeCurrentTabPage );
	}

	return 0;
}

static int AdujstPosition( struct TabPage *pnodeTabPage , int nAdujstPos )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GOTOPOS, nAdujstPos, 0 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_CHOOSECARETX, 0, 0 );
	return 0;
}

int AdujstPositionOnHexEditMode( struct TabPage *pnodeTabPage , MSG *p_msg )
{
	if( pnodeTabPage == NULL || pnodeTabPage->bHexEditMode == FALSE )
		return 0;

	int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	int nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	int nTotalLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETLINECOUNT, 0, 0 ) ;
	int nBeginPosInCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
	int nOffsetInCurrentLine = nCurrentPos - nBeginPosInCurrentLine ;

	if( nCurrentLine <= 1 )
	{
		if( 11 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 59 )
		{
			int nAdujstPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, 2, 0 ) + 12 ;
			AdujstPosition( pnodeTabPage , nAdujstPos );
		}
		else if( 61 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 78 )
		{
			int nAdujstPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, 2, 0 ) + 62 ;
			AdujstPosition( pnodeTabPage , nAdujstPos );
		}
	}
	else if( nOffsetInCurrentLine <= 10 )
	{
		int nAdujstPos = (12-nOffsetInCurrentLine) + nCurrentPos ;
		AdujstPosition( pnodeTabPage , nAdujstPos );
	}
	else if( nOffsetInCurrentLine == 11 )
	{
		if( nCurrentLine <= 2 )
		{
			int nAdujstPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, 2, 0 ) + 12 ;
			AdujstPosition( pnodeTabPage , nAdujstPos );
		}
		else
		{
			int nAdujstPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine-1, 0 ) + 58 ;
			AdujstPosition( pnodeTabPage , nAdujstPos );
		}
	}
	else if( 12 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 56 )
	{
		if( nCurrentLine+1 == nTotalLine && nOffsetInCurrentLine >= 12 + pnodeTabPage->nByteCountInLastLine*3 - 1 )
		{
			int nAdujstPos = nBeginPosInCurrentLine + 12 + (pnodeTabPage->nByteCountInLastLine-1)*3 + 1 ;
			AdujstPosition( pnodeTabPage , nAdujstPos );
		}
		else if( (nOffsetInCurrentLine-12)%3 == 2 )
		{
			if( p_msg->wParam == VK_LEFT )
			{
				int nAdujstPos = nCurrentPos-1 ;
				AdujstPosition( pnodeTabPage , nAdujstPos );
			}
			else
			{
				int nAdujstPos = nCurrentPos+1 ;
				AdujstPosition( pnodeTabPage , nAdujstPos );
			}
		}
	}
	else if( nOffsetInCurrentLine == 59 )
	{
		if( nCurrentLine+1 == nTotalLine || p_msg->wParam != VK_RIGHT )
		{
			int nAdujstPos = nCurrentPos - 1 ;
			AdujstPosition( pnodeTabPage , nAdujstPos );
		}
		else
		{
			int nAdujstPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine+1, 0 ) + 12 ;
			AdujstPosition( pnodeTabPage , nAdujstPos );
		}
	}
	else if( nOffsetInCurrentLine == 60 )
	{
		int nAdujstPos = (62-nOffsetInCurrentLine) + nCurrentPos ;
		AdujstPosition( pnodeTabPage , nAdujstPos );
	}
	else if( nOffsetInCurrentLine == 61 )
	{
		if( nCurrentLine == 2 )
		{
			int nAdujstPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, 2, 0 ) + 62 ;
			AdujstPosition( pnodeTabPage , nAdujstPos );
		}
		else
		{
			int nAdujstPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine-1, 0 ) + 77 ;
			AdujstPosition( pnodeTabPage , nAdujstPos );
		}
	}
	else if( nOffsetInCurrentLine >= 62 )
	{
		if( nCurrentLine+1 == nTotalLine && nOffsetInCurrentLine >= 62 + pnodeTabPage->nByteCountInLastLine - 1 )
		{
			int nAdujstPos = nBeginPosInCurrentLine + 62 + (pnodeTabPage->nByteCountInLastLine-1) ;
			AdujstPosition( pnodeTabPage , nAdujstPos );
		}
		else if( nOffsetInCurrentLine >= 78 )
		{
			if( nCurrentLine+1 == nTotalLine || p_msg->wParam != VK_RIGHT )
			{
				int nAdujstPos = nCurrentPos - (nOffsetInCurrentLine-77) ;
				AdujstPosition( pnodeTabPage , nAdujstPos );
			}
			else
			{
				int nAdujstPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine+1, 0 ) + 62 ;
				AdujstPosition( pnodeTabPage , nAdujstPos );
			}
		}
	}

	return 0;
}

int AdujstKeyOnHexEditMode( struct TabPage *pnodeTabPage , MSG *p_msg )
{
	if( p_msg->wParam == VK_BACK || p_msg->wParam == VK_RETURN || p_msg->wParam == VK_INSERT || p_msg->wParam == VK_DELETE )
	{
		return 1;
	}

	return 0;
}

int AdujstCharOnHexEditMode( struct TabPage *pnodeTabPage , MSG *p_msg )
{
	if( pnodeTabPage == NULL || pnodeTabPage->bHexEditMode == FALSE )
		return 0;

	int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	int nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	int nBeginPosInCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
	int nOffsetInCurrentLine = nCurrentPos - nBeginPosInCurrentLine ;

	if( 11 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 59 )
	{
		if( '0' <= p_msg->wParam && p_msg->wParam <= '9' )
		{
			;
		}
		else if( 'a' <= p_msg->wParam && p_msg->wParam <= 'z' )
		{
			p_msg->wParam = toupper((int)(p_msg->wParam)) ;
		}
		else if( 'A' <= p_msg->wParam && p_msg->wParam <= 'Z' )
		{
			;
		}
		else
		{
			return 1;
		}
	}
	else if( 61 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 78 )
	{
		if( 32 <= p_msg->wParam && p_msg->wParam <= 126 )
		{
			;
		}
		else
		{
			return 1;
		}
	}
	else
	{
		return 1;
	}

	return 0;
}

int SyncDataOnHexEditMode( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage == NULL || pnodeTabPage->bHexEditMode == FALSE )
		return 0;

	int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	int nCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_LINEFROMPOSITION, nCurrentPos, 0 ) ;
	int nBeginPosInCurrentLine = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_POSITIONFROMLINE, nCurrentLine, 0 ) ;
	int nOffsetInCurrentLine = nCurrentPos - nBeginPosInCurrentLine ;
	int nOffset ;
	unsigned char nByte1 , nByte2 , nByte ;
	unsigned char buf[2+1] ;

	if( 12 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 59 )
	{
		nOffset = (nOffsetInCurrentLine-1-12) / 3 ;
		nByte1 = (unsigned char)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCHARAT, nBeginPosInCurrentLine+12+nOffset*3, 0 ) ;
		nByte2 = (unsigned char)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCHARAT, nBeginPosInCurrentLine+12+nOffset*3+1, 0 ) ;
		HexByteFold( nByte1 , nByte2 , nByte );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETSEL , nBeginPosInCurrentLine+62+nOffset , nBeginPosInCurrentLine+62+nOffset+1 );
		if( 32 <= nByte && nByte <= 126 )
			buf[0] = nByte ;
		else
			buf[0] = '.' ;
		buf[1] = '\0' ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)buf );
		AdujstPosition( pnodeTabPage , nCurrentPos );
	}
	else if( 62 <= nOffsetInCurrentLine && nOffsetInCurrentLine <= 78 )
	{
		nOffset = (nOffsetInCurrentLine-1-62) ;
		nByte = (unsigned char)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla, SCI_GETCHARAT, nBeginPosInCurrentLine+62+nOffset, 0 ) ;
		HexByteExpand( nByte , buf );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETSEL , nBeginPosInCurrentLine+12+nOffset*3 , nBeginPosInCurrentLine+12+nOffset*3+2 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_REPLACESEL , 0 , (sptr_t)buf );
		AdujstPosition( pnodeTabPage , nCurrentPos );
	}

	return 0;
}

unsigned char *StrdupHexEditTextFold( struct TabPage *pnodeTabPage , size_t *pnTextLength )
{
	int nHexTextLength = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
	unsigned char *pcHexText = (unsigned char*)malloc( nHexTextLength + 1 ) ;
	if( pcHexText == NULL )
	{
		ErrorBox( "分配内存以存放十六进制数据失败" );
		return NULL;
	}
	memset( pcHexText , 0x00 , nHexTextLength + 1 );
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXT , (sptr_t)(nHexTextLength+1) , (sptr_t)pcHexText ) ;

	int nHexTextLineCount = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLINECOUNT , 0 , 0 ) ;
	size_t nTextLength = (nHexTextLineCount-3)*16 + pnodeTabPage->nByteCountInLastLine ;
	unsigned char *pcText = (unsigned char*)malloc( nTextLength + 1 ) ;
	if( pcText == NULL )
	{
		free( pcHexText );
		ErrorBox( "分配内存以存放文本数据失败" );
		return NULL;
	}
	memset( pcText , 0x00 , nTextLength + 1 );

	unsigned char *pcHexLineBegin = pcHexText ;
	unsigned char *pcTextOffset = pcText ;
	int y , x ;
	unsigned char *tmp = NULL ;
	for( y = 0 ; y < nHexTextLineCount ; y++ )
	{
		if( y > 1 )
		{
			for( x = 0 ; x < 16 ; x++ )
			{
				if( y+1 == nHexTextLineCount && x >= pnodeTabPage->nByteCountInLastLine )
					break;
				
				HexByteFold( pcHexLineBegin[12+x*3] , pcHexLineBegin[12+x*3+1] , (*pcTextOffset) );
				pcTextOffset++;
			}
		}
		if( y+1 == nHexTextLineCount )
			break;

		tmp = (unsigned char *)strchr( (char *)pcHexLineBegin , '\n' ) ;
		if( tmp == NULL )
		{
			ErrorBox( "内部错误：十六进制数据提前耗尽" );
			free( pcHexText );
			free( pcText );
			return NULL;
		}
		pcHexLineBegin = tmp + 1 ;
	}

	if( pcTextOffset-pcText != nTextLength )
	{
		ErrorBox( "内部错误：转换出来的数据的长度[%d]与估算值[%d]不一致" , pcTextOffset-pcText , nTextLength );
		free( pcHexText );
		free( pcText );
		return NULL;
	}

	free( pcHexText );

	if( pnTextLength )
		(*pnTextLength) = nTextLength ;
	return pcText;
}

int OnViewEnableWindowsVisualStyles()
{
	if( g_stEditUltraMainConfig.bEnableWindowsVisualStyles == FALSE )
	{
		g_stEditUltraMainConfig.bEnableWindowsVisualStyles = TRUE ;
		InfoBox( "已启用Windows主题样式，重启后生效" );
	}
	else
	{
		g_stEditUltraMainConfig.bEnableWindowsVisualStyles = FALSE ;
		InfoBox( "已禁用Windows主题样式，重启后生效" );
	}

	UpdateAllMenus( g_hwndMainWindow , g_pnodeCurrentTabPage );

	SaveMainConfigFile();

	return 0;
}

int OnViewTabWidth( struct TabPage *pnodeTabPage )
{
	char		acTabWidth[ 20 ] ;

	int		nret = 0 ;

	memset( acTabWidth , 0x00 , sizeof(acTabWidth) );
	snprintf( acTabWidth , sizeof(acTabWidth)-1 , "%d" , g_stEditUltraMainConfig.nTabWidth );
	nret = InputBox( g_hwndMainWindow , "请输入制表符宽度：" , "输入窗口" , 0 , acTabWidth , sizeof(acTabWidth)-1 ) ;
	if( nret == IDOK )
	{
		if( acTabWidth[0] )
		{
			int		nTabPagesCount ;
			int		nTabPageIndex ;
			TCITEM		tci ;
			struct TabPage	*p = NULL ;

			g_stEditUltraMainConfig.nTabWidth = atoi(acTabWidth) ;

			UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

			SaveMainConfigFile();

			nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
			for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
			{
				memset( & tci , 0x00 , sizeof(TCITEM) );
				tci.mask = TCIF_PARAM ;
				TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
				p = (struct TabPage *)(tci.lParam);
				p->pfuncScintilla( p->pScintilla , SCI_SETTABWIDTH , g_stEditUltraMainConfig.nTabWidth , 0 );
			}
		}
	}

	return 0;
}

int OnViewOnKeydownTabConvertSpaces( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bOnKeydownTabConvertSpaces == FALSE )
	{
		g_stEditUltraMainConfig.bOnKeydownTabConvertSpaces = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bOnKeydownTabConvertSpaces = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETUSETABS , (g_stEditUltraMainConfig.bOnKeydownTabConvertSpaces==TRUE?false:true) , 0 );
	}

	return 0;
}

int OnViewWrapLineMode( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bWrapLineMode == FALSE )
	{
		g_stEditUltraMainConfig.bWrapLineMode = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bWrapLineMode = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETWRAPMODE , (g_stEditUltraMainConfig.bWrapLineMode==TRUE?2:0) , 0 );
	}

	return 0;
}

int OnViewLineNumberVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bLineNumberVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bLineNumberVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bLineNumberVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETMARGINWIDTHN , MARGIN_LINENUMBER_INDEX , (g_stEditUltraMainConfig.bLineNumberVisiable==TRUE?MARGIN_LINENUMBER_WIDTH:0) );
	}

	return 0;
}

int OnViewBookmarkVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bBookmarkVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bBookmarkVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bBookmarkVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETMARGINWIDTHN , MARGIN_BOOKMARK_INDEX , (g_stEditUltraMainConfig.bBookmarkVisiable==TRUE?MARGIN_BOOKMARK_WIDTH:0) );
	}

	return 0;
}

int OnViewWhiteSpaceVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bWhiteSpaceVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bWhiteSpaceVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bWhiteSpaceVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETVIEWWS , (g_stEditUltraMainConfig.bWhiteSpaceVisiable==TRUE?SCWS_VISIBLEALWAYS:SCWS_INVISIBLE) , 0 );
		p->pfuncScintilla( p->pScintilla , SCI_SETWHITESPACESIZE , g_stEditUltraMainConfig.nWhiteSpaceSize , 0 );
		p->pfuncScintilla( p->pScintilla , SCI_SETTABDRAWMODE , SCTD_LONGARROW , 0 );
	}

	return 0;
}

int OnViewNewLineVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bNewLineVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bNewLineVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bNewLineVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETVIEWEOL , g_stEditUltraMainConfig.bNewLineVisiable , 0 );
	}

	return 0;
}

int OnViewIndentationGuidesVisiable( struct TabPage *pnodeTabPage )
{
	if( g_stEditUltraMainConfig.bIndentationGuidesVisiable == FALSE )
	{
		g_stEditUltraMainConfig.bIndentationGuidesVisiable = TRUE ;
	}
	else
	{
		g_stEditUltraMainConfig.bIndentationGuidesVisiable = FALSE ;
	}

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	SaveMainConfigFile();

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	UpdateAllMenus( g_hwndMainWindow , pnodeTabPage );

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		p->pfuncScintilla( p->pScintilla , SCI_SETINDENTATIONGUIDES , (g_stEditUltraMainConfig.bIndentationGuidesVisiable?SC_IV_LOOKBOTH:SC_IV_NONE) , 0 );
	}

	return 0;
}

int OnViewZoomOut( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_ZOOMIN , 0 , 0 );

	return 0;
}

int OnViewZoomIn( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_ZOOMOUT , 0 , 0 );

	return 0;
}

int OnViewZoomReset( struct TabPage *pnodeTabPage )
{
	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_SETZOOM , g_nZoomReset , 0 );

	return 0;
}

int OnEditorTextSelected( struct TabPage *pnodeTabPage )
{
	if( pnodeTabPage == NULL || pnodeTabPage->pstDocTypeConfig == NULL )
		return 0;
	if( pnodeTabPage->bHexEditMode == TRUE )
		return 0;

	size_t	nTextLenSelected = 0 ;
	char	*pcTextSelected = StrdupEditorSelection( & nTextLenSelected , 0 ) ;
	if( pcTextSelected )
	{
		pnodeTabPage->bHasTextSelected = TRUE ;

		int nSelectionStartPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETSELECTIONSTART , 0 , 0 ) ;

		int nTextTotalLength = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_INDICATORCLEARRANGE , 0 , nTextTotalLength );
		char *pcText = (char*)malloc( nTextTotalLength + 1 ) ;
		if( pcText == NULL )
			return -1;
		memset( pcText , 0x00 , nTextTotalLength + 1 );
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXT , (sptr_t)(nTextTotalLength+1) , (sptr_t)pcText ) ;

		char *p = strstr( pcText , pcTextSelected ) ;
		while( p )
		{

			if( p-pcText != nSelectionStartPos )
			{
				pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_INDICATORFILLRANGE , p-pcText  , nTextLenSelected ) ;
			}

			p = strstr( p+nTextLenSelected , pcTextSelected ) ;
		}

		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_INDICSETSTYLE , 0 , INDIC_ROUNDBOX ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_INDICSETFORE , 0 , g_pstWindowTheme->stStyleTheme.indicator.bgcolor ) ;
		pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_INDICSETALPHA , 0 , 100 ) ;

		free( pcTextSelected );
		free( pcText );
	}
	else
	{
		if( pnodeTabPage->bHasTextSelected == TRUE )
		{
			int nTextTotalLength = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
			pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_INDICATORCLEARRANGE , 0 , nTextTotalLength );

			pnodeTabPage->bHasTextSelected = FALSE ;
		}
	}

	return 0;
}
