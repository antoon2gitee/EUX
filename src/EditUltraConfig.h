#ifndef _H_EDITULTRA_CONFIG_
#define _H_EDITULTRA_CONFIG_

#include "framework.h"

#define FILEBUF_MAX	1024*1024

struct EditUltraMainConfig
{
	BOOL		bCreateNewFileOnNewBoot ;
	BOOL		bOpenFilesThatOpenningOnExit ;
	char		aacOpenFilesOnBoot[OPENFILES_ON_BOOT_MAXCOUNT][MAX_PATH] ;
	char		acActiveFileOnBoot[MAX_PATH] ;
	BOOL		bSetReadOnlyAfterOpenFile ;
	char		aacOpenPathFilenameRecently[OPEN_PATHFILENAME_RECENTLY_MAXCOUNT][MAX_PATH] ;
	BOOL		bCheckUpdateWhereSelectTabPage ;
	BOOL		bPromptWhereAutoUpdate ;
	int		nNewFileEols ;
	UINT		nNewFileEncoding ;
	BOOL		bEnableAutoAddCloseChar ;
	BOOL		bEnableAutoIdentation ;

	char		acWindowTheme[MAX_PATH] ;
	BOOL		bEnableWindowsVisualStyles ;
	BOOL		bLineNumberVisiable ;
	BOOL		bBookmarkVisiable ;
	BOOL		bWhiteSpaceVisiable ;
	int		nWhiteSpaceSize ;
	BOOL		bNewLineVisiable ;
	BOOL		bIndentationGuidesVisiable ;

	int		nTabWidth ;
	BOOL		bOnKeydownTabConvertSpaces ;

	BOOL		bWrapLineMode ;

	int		nFileTreeBarWidth ;
	int		nSymbolListWidth ;
	int		nSymbolTreeWidth ;
	int		nSqlQueryResultEditHeight ;
	int		nSqlQueryResultListViewHeight ;

	int		nReloadSymbolListOrTreeInterval ;
	BOOL		bBlockFoldVisiable ;
	BOOL		bEnableAutoCompletedShow ;
	int		nAutoCompletedShowAfterInputCharacters ;
	BOOL		bEnableCallTipShow ;

	char		acProcessFileCommand[ 1024 ] ;
	char		acProcessTextCommand[ 1024 ] ;
} ;

void SetEditUltraMainConfigDefault( struct EditUltraMainConfig *pstEditUltraConfig );

int LoadMainConfigFile( char *filebuf );
void FoldConfigStringItemValue( char *value );
int DecomposeConfigFileBuffer( char *filebuf , char **ppcItemName , char **ppcItemValue );
int LoadConfig();
void ExpandConfigStringItemValue( FILE *fp , char *value );
int SaveMainConfigFile();

#endif
