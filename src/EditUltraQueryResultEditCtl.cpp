#include "framework.h"

int CreateQueryResultEditCtl( struct TabPage *pnodeTabPage , HFONT hFont )
{
	if( pnodeTabPage->hwndQueryResultEdit )
	{
		DestroyWindow( pnodeTabPage->hwndQueryResultEdit );
	}

	/* 创建结果消息控件 */
	// pnodeTabPage->hwndQueryResultEdit = ::CreateWindow( "EDIT" , NULL , WS_BORDER|WS_CHILD|WS_VISIBLE|WS_VSCROLL|ES_LEFT|ES_MULTILINE|ES_AUTOVSCROLL , pnodeTabPage->rectQueryResultEdit.left , pnodeTabPage->rectQueryResultEdit.top , pnodeTabPage->rectQueryResultEdit.right-pnodeTabPage->rectQueryResultEdit.left , pnodeTabPage->rectQueryResultEdit.bottom-pnodeTabPage->rectQueryResultEdit.top , g_hwndMainClient , NULL , g_hAppInstance , NULL ) ; 
	pnodeTabPage->hwndQueryResultEdit = ::CreateWindow( "EDIT" , NULL , WS_CHILD|WS_VISIBLE|WS_VSCROLL|ES_LEFT|ES_MULTILINE|ES_AUTOVSCROLL , pnodeTabPage->rectQueryResultEdit.left , pnodeTabPage->rectQueryResultEdit.top , pnodeTabPage->rectQueryResultEdit.right-pnodeTabPage->rectQueryResultEdit.left , pnodeTabPage->rectQueryResultEdit.bottom-pnodeTabPage->rectQueryResultEdit.top , g_hwndMainWindow , NULL , g_hAppInstance , NULL ) ; 
	if( pnodeTabPage->hwndQueryResultEdit == NULL )
	{
		::MessageBox(NULL, TEXT("不能创建结果消息控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
		return -1;
	}

	SendMessage( pnodeTabPage->hwndQueryResultEdit , WM_SETFONT , (WPARAM)hFont, 0);

	return 0;
}

int AppendSqlQueryResultEditText( HWND hWnd , char *format , ... )
{
	va_list		valist ;
	SYSTEMTIME	systime ;
	char		buf[ 1024 ] ;
	int		l , len ;

	memset( buf , 0x00 , sizeof(buf) );
	len = 0 ;

	GetLocalTime( & systime );

	l = snprintf( buf+len , sizeof(buf)-1-len , "%04d-%02d-%02d %02d:%02d:%02d.%06d | " , systime.wYear , systime.wMonth , systime.wDay , systime.wHour , systime.wMinute , systime.wSecond , systime.wMilliseconds*1000 ) ;
	if( l > 0 )
		len += l ;

	va_start( valist , format );
	l = vsnprintf( buf+len , sizeof(buf)-1-len , format , valist ) ;
	if( l > 0 )
		len += l ;
	va_end( valist );

	l = snprintf( buf+len , sizeof(buf)-1-len , "\r\n" ) ;
	if( l > 0 )
		len += l ;

	SendMessage( hWnd , EM_SETSEL , -2 , -1 );
	SendMessage( hWnd , EM_REPLACESEL , true , (LPARAM)buf );

	return len;
}

