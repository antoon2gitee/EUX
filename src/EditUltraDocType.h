#ifndef _H_EDITULTRA_DOCTYPE_
#define _H_EDITULTRA_DOCTYPE_

#include "framework.h"

extern uintptr_t		ReloadSymbolListOrTreeThreadHandler ;

struct AutoCompletedShowNode
{
	char		*acAutoCompletedString ;
	int		begin ;
	int		end ;

	struct rb_node	nodeAutoCompletedShow ;
} ;

struct AutoCompletedShowTree
{
	struct rb_root	treeAutoCompletedShow ;
} ;

int LinkAutoCompletedShowTreeNode( struct AutoCompletedShowTree *tree , struct AutoCompletedShowNode *node );
void UnlinkAutoCompletedShowTreeNode( struct AutoCompletedShowTree *tree , struct AutoCompletedShowNode *node );
int CompareAutoCompletedShowTreeNodePartlyEntry( void *pv1 , void *pv2 );
struct AutoCompletedShowNode *QueryAutoCompletedShowTreeNodePartly( struct AutoCompletedShowTree *tree , struct AutoCompletedShowNode *node );
void DestroyAutoCompletedShowTree( struct AutoCompletedShowTree *tree );

struct CallTipShowNode
{
	char		*acCallTipFuncName ;
	char		*acCallTipFuncDesc ;

	struct rb_node	nodeCallTipShow ;
};

struct CallTipShowTree
{
	struct rb_root	treeCallTipShow ;
};

int LinkCallTipShowTreeNode( struct CallTipShowTree *tree , struct CallTipShowNode *node );
void UnlinkCallTipShowTreeNode( struct CallTipShowTree *tree , struct CallTipShowNode *node );
struct CallTipShowNode *QueryCallTipShowTreeNode( struct CallTipShowTree *tree , struct CallTipShowNode *node );
void DestroyCallTipShowTree( struct CallTipShowTree *tree );

typedef int funcInitTabPageControlsBeforeLoadFile( struct TabPage *pnodeTabPage );
typedef int funcInitTabPageControlsAfterLoadFile( struct TabPage *pnodeTabPage );
typedef int funcParseFileConfigHeader( struct TabPage *pnodeTabPage );
typedef int funcOnCharAdded( struct TabPage *pnodeTabPage , SCNotification *lpnotify );
typedef int funcOnKeyDown( struct TabPage *pnodeTabPage , WPARAM wParam , LPARAM lParam );
typedef int funcOnKeyUp( struct TabPage *pnodeTabPage , WPARAM wParam , LPARAM lParam );
typedef int funcOnReloadSymbolList( struct TabPage *pnodeTabPage );
typedef int funcOnDbClickSymbolList( struct TabPage *pnodeTabPage );
typedef int funcOnReloadSymbolTree( struct TabPage *pnodeTabPage );
typedef int funcOnDbClickSymbolTree( struct TabPage *pnodeTabPage );
typedef int funcCleanTabPageControls( struct TabPage *pnodeTabPage );

enum DocType
{
	DOCTYPE_END = 0 ,
	DOCTYPE_TXT ,
	DOCTYPE_CPP ,
	DOCTYPE_CS ,
	DOCTYPE_JAVA ,
	DOCTYPE_JAVASCRIPT ,
	DOCTYPE_GO ,
	DOCTYPE_SWIFT ,
	DOCTYPE_SQL ,
	DOCTYPE_REDIS ,
	DOCTYPE_PYTHON ,
	DOCTYPE_LUA ,
	DOCTYPE_PERL ,
	DOCTYPE_SH ,
	DOCTYPE_RUST ,
	DOCTYPE_RUBY ,
	DOCTYPE_LISP ,
	DOCTYPE_ASM ,
	DOCTYPE_COBOL ,
	DOCTYPE_HTML ,
	DOCTYPE_XML ,
	DOCTYPE_CSS ,
	DOCTYPE_JSON ,
	DOCTYPE_YAML ,
	DOCTYPE_MAKEFILE ,
	DOCTYPE_CMAKE ,
	DOCTYPE_MARKDOWN ,
	DOCTYPE_LOG ,
	DOCTYPE_PROPERTIES ,
	DOCTYPE_BATCH
} ;

struct DocTypeConfig
{
	enum DocType				nDocType ;
	const char				*pcFileTypeConfigFilename ;
	const char				*pcFileExtnames ;
	const char				*pcFileTypeDesc ;

	funcInitTabPageControlsBeforeLoadFile	*pfuncInitTabPageControlsBeforeLoadFile ;
	funcInitTabPageControlsAfterLoadFile	*pfuncInitTabPageControlsAfterLoadFile ;
	funcParseFileConfigHeader		*pfuncParseFileConfigHeader ;
	funcOnKeyDown				*pfuncOnKeyDown ;
	funcOnKeyDown				*pfuncOnKeyUp ;
	funcOnCharAdded				*pfuncOnCharAdded ;
	funcOnReloadSymbolList			*pfuncOnReloadSymbolList ;
	funcOnDbClickSymbolList			*pfuncOnDbClickSymbolList ;
	funcOnReloadSymbolTree			*pfuncOnReloadSymbolTree ;
	funcOnDbClickSymbolTree			*pfuncOnDbClickSymbolTree ;
	funcCleanTabPageControls		*pfuncCleanTabPageControls ;

	char				*keywords ;
	char				*keywords2 ;
	char				*keywords3 ;
	char				*keywords4 ;
	char				*keywords5 ;
	char				*keywords6 ;
	char				*autocomplete_set ;
	char				*autocomplete2_set ;

	struct AutoCompletedShowTree	stAutoCompletedShowTree ;
	size_t				sAutoCompletedBufferSize ;
	char				*acAutoCompletedBuffer ;
	char				*acAutoCompletedShowBuffer ;
	int				nAutoCompletedShowStartPos ;
	int				nAutoCompletedShowEndPos ;

	struct AutoCompletedShowTree	stAutoCompletedShowTree2 ;
	size_t				sAutoCompletedBufferSize2 ;
	char				*acAutoCompletedBuffer2 ;
	char				*acAutoCompletedShowBuffer2 ;
	int				nAutoCompletedShowStartPos2 ;
	int				nAutoCompletedShowEndPos2 ;

	struct CallTipShowTree		stCallTipShowTree ;

	char				*acSymbolReqularExp ;
} ;

extern struct DocTypeConfig	g_astDocTypeConfig[] ;

struct DocTypeConfig *GetDocTypeConfig( char *pcExtname );
char *GetFileDialogFilterPtr();

int BuildAutoCompletedShowTree( struct DocTypeConfig *pstFileExtnameMapper , char *word2 );
int ExpandAutoCompletedShowTreeToBuffer( struct DocTypeConfig *pstFileExtnameMapper );

int BuildAutoCompletedShowTree2( struct DocTypeConfig *pstFileExtnameMapper , char *word2 );
int ExpandAutoCompletedShowTreeToBuffer2( struct DocTypeConfig *pstFileExtnameMapper );

int LoadLexerConfigFile( const char *filename , struct DocTypeConfig *pstFileExtnameMapper , char *filebuf );

#define CONFIG_KEY_MATERIAL_LEXER	"DOCTYP"

int BeginReloadSymbolListOrTreeThread();
void ReloadSymbolListOrTreeThreadEntry( void *param );

void UpdateFileTypeMenu();

#endif
