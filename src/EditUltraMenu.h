#ifndef _H_EDITULTRA_MENU_
#define _H_EDITULTRA_MENU_

#include "framework.h"

void PushOpenPathFilenameRecently( char *acPathFilename );
void UpdateOpenPathFilenameRecently();
void SetCurrentWindowThemeChecked( HWND hWnd );
void SetViewTabWidthMenuText( int nMenuId );
void SetSourceCodeAutoCompletedShowAfterInputCharactersMenuText( int nMenuId );
void UpdateAllMenus( HWND hWnd , struct TabPage *pnodeTabPage );

#endif
