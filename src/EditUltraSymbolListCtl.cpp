#include "framework.h"

int IfSymbolReqularExp_CreateSymbolListCtl( struct TabPage *pnodeTabPage , HFONT hFont )
{
	if( pnodeTabPage->pstDocTypeConfig->acSymbolReqularExp )
	{
		if( pnodeTabPage->hwndSymbolList )
		{
			DestroyWindow( pnodeTabPage->hwndSymbolList );
		}

		pnodeTabPage->hwndSymbolList = ::CreateWindow( "listbox" , NULL , WS_CHILD|WS_VSCROLL|LBS_NOTIFY|LBS_USETABSTOPS , pnodeTabPage->rectSymbolList.left , pnodeTabPage->rectSymbolList.top , pnodeTabPage->rectSymbolList.right-pnodeTabPage->rectSymbolList.left , pnodeTabPage->rectSymbolList.bottom-pnodeTabPage->rectSymbolList.top , g_hwndMainWindow , NULL , g_hAppInstance , NULL ) ; 
		if( pnodeTabPage->hwndSymbolList == NULL )
		{
			::MessageBox(NULL, TEXT("不能创建符号列表控件"), TEXT("错误"), MB_ICONERROR | MB_OK);
			return -1;
		}

		::SendMessage( pnodeTabPage->hwndSymbolList , WM_SETFONT , (WPARAM)hFont, 0);

		::SendMessage( pnodeTabPage->hwndSymbolList , TVM_SETTEXTCOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.color );
		::SendMessage( pnodeTabPage->hwndSymbolList , TVM_SETLINECOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.color );
		::SendMessage( pnodeTabPage->hwndSymbolList , TVM_SETBKCOLOR , 0 , g_pstWindowTheme->stStyleTheme.text.bgcolor );

		// 预编译符号列表
		if( pnodeTabPage->pcreSymbolRe )
			pcre_free( pnodeTabPage->pcreSymbolRe );
		const char *error_desc = NULL ;
		int error_offset ;
		pnodeTabPage->pcreSymbolRe = pcre_compile( pnodeTabPage->pstDocTypeConfig->acSymbolReqularExp , PCRE_MULTILINE , & error_desc , & error_offset , NULL ) ;
		if( pnodeTabPage->pcreSymbolRe == NULL )
		{
			::MessageBox(NULL, "不能解析符号正则表达式", TEXT("错误"), MB_ICONERROR | MB_OK);
			return 1;
		}
	}

	return 0;
}

int ReloadSymbolList_WithReqularExp( struct TabPage *pnodeTabPage )
{
	int		nFileSize ;
	char		*acFileBuffer = NULL ;
	char		*p1 = NULL ;
	char		*p2 = NULL ;
	char		*p3 = NULL ;
	int		nLineNo ;

	ListBox_ResetContent( pnodeTabPage->hwndSymbolList );

	nFileSize = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETLENGTH , 0 , 0 ) ;
	acFileBuffer = (char*)malloc( nFileSize + 1 ) ;
	if( acFileBuffer == NULL )
		return -1;
	memset( acFileBuffer , 0x00 , nFileSize + 1 );

	pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETTEXT , (sptr_t)(nFileSize+1) , (sptr_t)acFileBuffer ) ;

	p1 = acFileBuffer ;
	nLineNo = 0 ;
	while(1)
	{
		for( p3 = p1 ; *(p3) && *(p3) != '\n' ; p3++ )
			;
		if( *(p3) == '\0' )
			break;
		*(p3) = '\0' ;

		p2 = p3 ;
		if( p2 > p1 && *(p2-1) == '\r' )
		{
			p2--;
			*(p2) = '\0' ;
		}

		int	nSymbolOvector[ PATTERN_OVECCOUNT ] ;
		int	nSymbolOvectorCount ;
		int	tmp = 0 ;
		nSymbolOvectorCount = pcre_exec( pnodeTabPage->pcreSymbolRe , NULL , p1 , (int)(p2-p1) , 0 , 0 , nSymbolOvector , PATTERN_OVECCOUNT ) ;
		if( nSymbolOvectorCount > 0 )
		{
			memmove( p1 , p1+nSymbolOvector[2] , nSymbolOvector[3]-nSymbolOvector[2] );
			*(p1+nSymbolOvector[3]-nSymbolOvector[2]) = '\0' ;

			if( STRCMP( p1 , != , "if" ) )
			{
				int nListBoxItemIndex = ListBox_AddString( pnodeTabPage->hwndSymbolList , p1 ) ;
				ListBox_SetItemData( pnodeTabPage->hwndSymbolList , nListBoxItemIndex , (LPARAM)nLineNo );
			}
		}

		p1 = p3 + 1 ;
		nLineNo++;
	}

	free( acFileBuffer );

	return 0;
}

int GetCurrentWordAndJumpGotoLine( struct TabPage *pnodeTabPage )
{
	int		nCurrentPos ;
	int		nCurrentWordStartPos ;
	int		nCurrentWordEndPos ;
	char		acCurrentWord[ 256 ] ;

	int		nTabPagesCount ;
	int		nTabPageIndex ;
	TCITEM		tci ;
	struct TabPage	*p = NULL ;

	nCurrentPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_GETCURRENTPOS, 0, 0 ) ;
	nCurrentWordStartPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDSTARTPOSITION, nCurrentPos, true ) ;
	nCurrentWordEndPos = (int)g_pnodeCurrentTabPage->pfuncScintilla( g_pnodeCurrentTabPage->pScintilla, SCI_WORDENDPOSITION, nCurrentPos, true ) ;
	if( nCurrentWordEndPos - nCurrentWordStartPos > sizeof(acCurrentWord)-1 )
		nCurrentWordEndPos = nCurrentWordStartPos + sizeof(acCurrentWord)-1 ;
	memset( acCurrentWord , 0x00 , sizeof(acCurrentWord) );
	GetTextByRange( g_pnodeCurrentTabPage , nCurrentWordStartPos , nCurrentWordEndPos , acCurrentWord );

	nTabPagesCount = TabCtrl_GetItemCount( g_hwndTabPages ) ;
	for( nTabPageIndex = 0; nTabPageIndex < nTabPagesCount; nTabPageIndex++ )
	{
		memset( & tci , 0x00 , sizeof(TCITEM) );
		tci.mask = TCIF_PARAM ;
		TabCtrl_GetItem( g_hwndTabPages , nTabPageIndex , & tci );
		p = (struct TabPage *)(tci.lParam);
		if( p->pstDocTypeConfig->nDocType == DOCTYPE_CPP )
		{
			int nFindIndex = ListBox_FindString( p->hwndSymbolList , 0 , acCurrentWord ) ;
			if( nFindIndex != LB_ERR )
			{
				if( p != pnodeTabPage )
					SelectTabPageByIndex( nTabPageIndex );
				int nGotoLineNo = (int)SendMessage( p->hwndSymbolList , LB_GETITEMDATA , nFindIndex , 0 ) ;
				AddNavigateBackNextTrace( p , nCurrentPos );
				JumpGotoLine( p , nGotoLineNo , 0 );
				break;
			}
		}
	}

	return 0;
}

int GetSymbolListItemAndJumpGotoLine( struct TabPage *pnodeTabPage )
{
	int nListBoxItemNo = (int)SendMessage( pnodeTabPage->hwndSymbolList , LB_GETCURSEL , 0 , 0 ) ;
	int nGotoLineNo = (int)SendMessage( pnodeTabPage->hwndSymbolList , LB_GETITEMDATA , nListBoxItemNo , 0 ) ;
	int nCurrentPos = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_GETCURRENTPOS , 0 , 0 ) ;
	int nCurrentLineNo = (int)pnodeTabPage->pfuncScintilla( pnodeTabPage->pScintilla , SCI_LINEFROMPOSITION , nCurrentPos , 0 ) ;
	AddNavigateBackNextTrace( g_pnodeCurrentTabPage , nCurrentPos );
	JumpGotoLine( pnodeTabPage , nGotoLineNo , nCurrentLineNo );

	return 0;
}
